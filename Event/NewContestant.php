<?php

namespace ATM\CompetitionBundle\Event;

use Symfony\Component\EventDispatcher\Event;


class NewContestant extends Event{

    const NAME = 'atm_competition_new_contestant.event';

    private $contestant;
    private $competition;

    public function __construct($contestant,$competition)
    {
        $this->contestant = $contestant;
        $this->competition = $competition;
    }

    public function getContestant()
    {
        return $this->contestant;
    }

    public function setContestant($contestant)
    {
        $this->contestant = $contestant;
    }

    public function getCompetition()
    {
        return $this->competition;
    }

    public function setCompetition($competition)
    {
        $this->competition = $competition;
    }
}