<?php

namespace ATM\CompetitionBundle\Controller;

use ATM\CompetitionBundle\Document\CompetitionIpRequest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use ATM\CompetitionBundle\Entity\AnonymousUser;
use ATM\CompetitionBundle\Entity\Image;
use ATM\CompetitionBundle\Entity\Contestant;
use ATM\CompetitionBundle\Document\CompetitionVote;
use ATM\CompetitionBundle\Services\SearchIpRequests;
use ATM\CompetitionBundle\Services\ImageManager;
use ATM\CompetitionBundle\Services\SearchVotes;
use ATM\CompetitionBundle\Services\SearchCompetition;
use ATM\CompetitionBundle\Services\SearchContestants;
use ATM\CompetitionBundle\Event\NewContestant;
use \DateTime;

class FrontendController extends Controller
{
    /**
     * @Route("/competition/{canonical}/{competitionId}/{page}", name="atm_competition_landing", defaults={"page":1})
     */
    public function landingAction($canonical,$competitionId, $page)
    {
        $em = $this->getDoctrine()->getManager();
        $competition = $em->getRepository('ATMCompetitionBundle:Competition')->findOneById($competitionId);

        if($competition){
            $config = $this->getParameter('atm_competition_config');

            return $this->render('ATMCompetitionBundle:Frontend:'.$canonical.'.html.twig',array(
                'competition' => $competition,
                'contestant_roles' => $config['contestant_roles'],
                'page' => $page
            ));
        }else{
            return $this->render('ATMCompetitionBundle:Frontend:competition_not_found.html.twig');
        }
    }

    /**
     * @Route("/contestant/join/competition/{competitionId}", name="atm_competition_contestant_join")
     */
    public function competitionContestantJoinAction($competitionId){
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $competition = $em->getRepository('ATMCompetitionBundle:Competition')->findOneById($competitionId);

        $contestant = $em->getRepository('ATMCompetitionBundle:Contestant')->findOneBy(array(
            'competition' =>$competitionId,
            'user' => $user->getId()
        ));

        if(!is_null($contestant)){

            $this->get('session')->getFlashBag()->set('atm_competition_not_allowed','You are already a contestant in this competition');

            return new RedirectResponse($this->get('router')->generate('atm_competition_landing',array(
                'competitionId' => $competitionId,
                'canonical' => $competition->getCanonical()
            )));
        }
        $config = $this->getParameter('atm_competition_config');

        //if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY') && in_array($config['contestant_role'],$user->getRoles())){
        if($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY') && !empty(array_intersect($config['contestant_roles'], $user->getRoles()))){
            $request = $this->get('request_stack')->getCurrentRequest();

            if($request->getMethod() == 'POST'){
                $contestant = new Contestant();
                $contestant->setUser($user);
                $contestant->setName($user->getUsername());
                $contestant->setEmail($user->getEmail());
                $contestant->setCompetition($competition);

                if(!is_null($config['media_entity']['namespace'])){
                    $mediaId = $request->get('user_media');
                    $media = $em->getRepository($config['media_entity']['namespace'])->findOneById($mediaId);
                    $contestant->setMedia($media);
                }

                $em->persist($contestant);

                $userFolder = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$competition->getFolder().'/'.$user->getUsernameCanonical();
                if(!is_dir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$competition->getFolder())){
                    mkdir($this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$competition->getFolder());
                }

                if(!is_dir($userFolder)){
                    mkdir($userFolder);
                }

                $mainImageFile = $request->files->get('main_image');
                $mainImageFilename= $this->get(ImageManager::class)->cropImage($mainImageFile,$userFolder,false,true);

                $mainImage = new Image();
                $mainImage->setPath($config['media_folder'].'/'.$competition->getFolder().'/'.$user->getUsernameCanonical().'/'.$mainImageFilename);
                $mainImage->setIsMain(true);
                $mainImage->setContestant($contestant);
                $em->persist($mainImage);

                $images = $request->files->get('images');
                if($images){
                    foreach($images as $img){
                        $filename = md5(uniqid()).'.'.$img->guessExtension();
                        $img->move($userFolder,$filename);
                        $this->get(ImageManager::class)->watermarkImage(
                            $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$competition->getFolder().'/'.$user->getUsernameCanonical().'/'.$filename,
                            $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$competition->getFolder().'/'.$user->getUsernameCanonical().'/'.$filename
                        );

                        $image = new Image();
                        $image->setPath($config['media_folder'].'/'.$competition->getFolder().'/'.$user->getUsernameCanonical().'/'.$filename);
                        $image->setContestant($contestant);
                        $em->persist($image);
                    }
                }

                $em->flush();

                $event = new NewContestant($contestant,$competition);
                $this->get('event_dispatcher')->dispatch(NewContestant::NAME, $event);

                return new RedirectResponse($this->get('router')->generate('atm_competition_landing',array(
                    'competitionId' => $competitionId,
                    'canonical' => $competition->getCanonical()
                )));
            }

            $params = array('ids' => array($competitionId));

            $competitions = $this->get(SearchCompetition::class)->search($params);

            return $this->render('ATMCompetitionBundle:Frontend:upload_images.html.twig',array(
                'competitionId' => $competitionId,
                'competition' => $competitions['results'][0],
                'media_namespace' => $config['media_entity']['namespace']
            ));
        }else{
            $this->get('session')->getFlashBag()->set('atm_competition_not_allowed',$config['messages']['not_allowed']);
            return new RedirectResponse($this->get('router')->generate('atm_competition_landing',array('competitionId'=> $competitionId,'canonical'=>$competition->getCanonical())));
        }
    }


    public function searchContestantsAction($params){

        $config = $this->getParameter('atm_competition_config');
        $contestants = $this->get(SearchContestants::class)->search($params);

        return $this->render('ATMCompetitionBundle:Frontend:search_results.html.twig',array(
            'contestants' => $contestants['results'],
            'pagination' => $contestants['pagination'],
            'competition_id' => $params['competition'],
            'contestant_roles' => $config['contestant_roles'],
        ));
    }

    /**
     * @Route("/contestant/details/{contestantId}", name="atm_competition_contestant_details")
     */
    public function contestantDetailsAction($contestantId){
        $params = array(
            'ids' => array($contestantId)
        );
        $contestant = $this->get(SearchContestants::class)->search($params);

        return $this->render('ATMCompetitionBundle:Frontend:contestant_details.html.twig',array(
            'contestant' => $contestant['results'][0]
        ));
    }

    /**
     * @Route("/check/email/{email}", name="atm_competition_check_email", options={"expose"=true})
     */
    public function checkEmailAction($email){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://disposable.debounce.io/?email='.$email
        ));

        $result = json_decode(curl_exec($curl),true);
        $validEmail = $result['disposable'] == 'false' ? false : true;

        if($validEmail === false){
            $this->get('session')->set('atm_competition_email_ok',$email);
        }


        return new Response($validEmail === true ? 'ko' : 'ok');
    }

    /**
     * @Route("/anonymous/user/send/email/{email}/{contestantId}", name="atm_competition_anonymous_user_send_mail", options={"expose"=true})
     */
    public function anonymousUserSendVoteLinkAction($email,$contestantId){
        $config = $this->getParameter('atm_competition_config');
        $contestant = $this->get(SearchContestants::class)->search(array('email'=>$email,'ids'=> $contestantId));
        $competition = $contestant['results'][0]['competition'];

        $request = $this->get('request_stack')->getCurrentRequest();
        $ip = $request->getClientIp();
        $currentDate = new DateTime();
        $currentDate->setTime('0','0','0');
        $numberIpRequest = $this->get(SearchIpRequests::class)->search($ip,$currentDate);

        if($numberIpRequest > 0){
            $this->get('session')->getFlashBag()->set('atm_competition_already_voted',$config['messages']['already_voted']);
            return new RedirectResponse($this->get('router')->generate('atm_competition_landing',array(
                'canonical' => $competition['canonical'],
                'competitionId' => $competition['id']
            )));
        }else{
            $dm = $this->get('doctrine_mongodb')->getManager();
            $ipRequest = new CompetitionIpRequest();
            $ipRequest->setIp($ip);
            $dm->persist($ipRequest);
            $dm->flush();
        }

        if($this->get('session')->has('atm_competition_email_ok')){
            $this->get('session')->remove('atm_competition_email_ok');

            $vote = $this->get(SearchVotes::class)->search(array(
                'email' => $email,
                'count' => true,
                'competition_id' => $competition['id']
            ));

            if($vote['count'] == 0){
                $this->get('atm_mailer')->send(
                    $email,
                    $config['email_subject'],
                    $this->renderView('ATMCompetitionBundle:Mail:'.$competition['canonical'].'_vote_link.html.twig',array(
                        'vote_link' => 'https://'.$config['site_domain'].$this->get('router')->generate('atm_competition_anonymous_user_vote',array(
                                'email' => $email,
                                'contestantId' => $contestantId
                            )),
                        'competition' => $competition['name']
                    ))
                );

                $this->get('session')->getFlashBag()->set('atm_competition_vote_link_sent',$config['messages']['vote_link_sent']);
            }else{
                $this->get('session')->getFlashBag()->set('atm_competition_already_voted',$config['messages']['already_voted']);
            }
        }else{
            $this->get('session')->getFlashBag()->set('atm_competition_email_not_validated',$config['messages']['email_not_validated']);
        }

        return new RedirectResponse($this->get('router')->generate('atm_competition_landing',array(
            'canonical' => $competition['canonical'],
            'competitionId' => $competition['id']
        )));
    }

    /**
     * @Route("/anonymous/user/vote/{email}/{contestantId}", name="atm_competition_anonymous_user_vote", options={"expose"=true})
     */
    public function anonymousUserVoteAction($email,$contestantId){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_competition_config');
        $contestant = $this->get(SearchContestants::class)->search(array('ids' => array($contestantId)));
        $competition = $contestant['results'][0]['competition'];
        $competition = $em->getRepository('ATMCompetitionBundle:Competition')->findOneById($competition['id']);


        $vote = $this->get(SearchVotes::class)->search(array(
            'email' => $email,
            'contestant_id' => $contestantId,
            'competition_id' => $competition->getId(),
            'count' => true
        ));

        if($vote['count'] == 0 ){
            $dm = $this->get('doctrine_mongodb')->getManager();

            $vote = new CompetitionVote();
            $vote->setEmail($email);
            $vote->setContestantId($contestantId);
            $vote->setCompetitionId($competition->getId());
            $dm->persist($vote);
            $dm->flush();

            $anonymousUser = new AnonymousUser();
            $anonymousUser->setCompetition($competition);
            $anonymousUser->setEmail($email);
            $em->persist($anonymousUser);
            $em->flush();

            $this->get('session')->getFlashBag()->set('atm_competition_voted_successfully',$config['messages']['voted_successfully']);
        }else{
            $this->get('session')->getFlashBag()->set('atm_competition_already_voted',$config['messages']['already_voted']);
        }

        return new RedirectResponse($this->get('router')->generate('atm_competition_landing',array(
            'canonical' => $competition->getCanonical(),
            'competitionId' => $competition->getId()
        )));
    }

    /**
     * @Route("/user/vote/{contestantId}", name="atm_competition_user_vote", options={"expose"=true})
     */
    public function userVoteAction($contestantId){


        $config = $this->getParameter('atm_competition_config');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $contestant = $this->get(SearchContestants::class)->search(array('ids' => array($contestantId)));
        $competition = $contestant['results'][0]['competition'];

        if($this->userCanVote($competition['id'])){
            $currentDate = new \DateTime();
            $minDate = \DateTime::createFromFormat('d-m-Y H:i:s',$currentDate->format('d-m-Y').' 00:00:00');
            $maxDate = \DateTime::createFromFormat('d-m-Y H:i:s',$currentDate->format('d-m-Y').' 23:59:59');

            $vote = $this->get(SearchVotes::class)->search(array(
                'email' => $user->getEmail(),
                'count' => true,
                'date_limit' => array(
                    'min' => $minDate,
                    'max' => $maxDate
                )
            ));

            if($vote['count'] == 0){
                $dm = $this->get('doctrine_mongodb')->getManager();
                $vote = new CompetitionVote();
                $vote->setEmail($user->getEmail());
                $vote->setContestantId($contestantId);
                $vote->setCompetitionId($competition['id']);
                $dm->persist($vote);
                $dm->flush();

                $this->get('session')->getFlashBag()->set('atm_competition_voted_successfully',$config['messages']['voted_successfully']);
            }else{
                $this->get('session')->getFlashBag()->set('atm_competition_already_voted',$config['messages']['already_voted']);
            }
        }else{
            $this->get('session')->getFlashBag()->set('atm_competition_already_voted','The competition is not active yet.');
        }



        return new RedirectResponse($this->get('router')->generate('atm_competition_landing',array(
            'canonical' => $competition['canonical'],
            'competitionId' => $competition['id']
        )));
    }

    private function userCanVote($competitionId){
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $qb
            ->select('partial c.{id,register_init_date,register_end_date}')
            ->from('ATMCompetitionBundle:Competition','c')
            ->where($qb->expr()->eq('c.id',$competitionId));

        $result = $qb->getQuery()->getArrayResult();

        if(isset($result[0])){
            $initTs = $result[0]['register_init_date']->getTimestamp();
            $endTs = $result[0]['register_end_date']->getTimestamp();

            $currentDate = new DateTime();
            $currentTs = $currentDate->getTimestamp();

            if($currentTs >= $initTs && $currentTs <= $endTs){
                return false;
            }
            return true;
        }
    }
}
