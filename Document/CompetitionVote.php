<?php

namespace ATM\CompetitionBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use \DateTime;

/**
 * @ODM\Document
 */
class CompetitionVote{

    /**
     * @ODM\Id(strategy="auto")
     */
    private $id;

    /**
     * @ODM\Field(type="string")
     */
    private $email;

    /**
     * @ODM\Field(type="integer")
     */
    private $competition_id;

    /**
     * @ODM\Field(type="date")
     */
    private $creation_date;

    /**
     * @ODM\Field(type="integer")
     */
    private $contestant_id;

    public function __construct()
    {
        $this->creation_date = new DateTime();
    }


    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getCompetitionId()
    {
        return $this->competition_id;
    }

    public function setCompetitionId($competition_id)
    {
        $this->competition_id = $competition_id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getContestantId()
    {
        return $this->contestant_id;
    }

    public function setContestantId($contestant_id)
    {
        $this->contestant_id = $contestant_id;
    }
}