<?php

namespace ATM\CompetitionBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use \DateTime;

/**
 * @ODM\Document
 */
class CompetitionIpRequest{
    /**
     * @ODM\Id(strategy="auto")
     */
    private $id;

    /**
     * @ODM\Field(type="string")
     */
    private $ip;

    /**
     * @ODM\Field(type="date")
     */
    private $creation_date;

    public function __construct()
    {
        $this->creation_date = new DateTime();
        $this->creation_date->setTime('0','0','0');
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }
}