<?php

namespace ATM\CompetitionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use ATM\CompetitionBundle\Entity\Competition;
use XLabs\TrumboWYGBundle\Form\TrumboWYGType;

class CompetitionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,array(
                'required' => true,
                'attr' => array(
                    'placeholder' => 'Name',
                    'autocomplete' => 'off'
                )
            ))
            ->add('init_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Voting starts',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('end_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Voting ends',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('register_init_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Register init Date',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('register_end_date',DateTimeType::class, array(
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd-MM-yyyy',
                'attr' => array(
                    'class' => 'datepicker',
                    'placeholder' => 'Register end Date',
                    'autocomplete' => 'off'
                ),
            ))
            ->add('rules', TrumboWYGType::class,array(
                'required' => false,
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Rules'
                ),
                'plugin_options' => [
                    'btns' => [['bold', 'italic', 'underline'], ['link'], ['viewHTML']],
                    'autogrow' => true,
                    'tagsToRemove' => ['script', 'link', 'iframe']
                ]
            ))
            ->add('prices', TrumboWYGType::class,array(
                'required' => false,
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Prices'
                ),
                'plugin_options' => [
                    'btns' => [['bold', 'italic', 'underline'], ['link'], ['viewHTML']],
                    'autogrow' => true,
                    'tagsToRemove' => ['script', 'link', 'iframe']
                ]
            ))
            //->add('rules',TextareaType::class,array('required'=>false,'attr'=> array('placeholder'=>'Rules')))
            //->add('prices',TextareaType::class,array('required'=>false,'attr'=> array('placeholder'=>'Prices')))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Competition::class,
        ));
    }

    public function getName()
    {
        return 'atmcompetition_bundle_product_type';
    }
}