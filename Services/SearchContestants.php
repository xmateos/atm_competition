<?php

namespace ATM\CompetitionBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use ATM\CompetitionBundle\Services\SearchVotes;

class SearchContestants{
    private $em;
    private $paginator;
    private $searchVotes;
    private $config;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $paginator,SearchVotes $searchVotes ,$config)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->searchVotes = $searchVotes;
        $this->config;
    }

    public function search($options){
        $defaultOptions = array(
            'name' => null,
            'date' => null,
            'ids' => null,
            'competition' => null,
            'pagination' => null,
            'page' => 1,
            'order_by_field' => 'name',
            'order_by_direction' => 'DESC',
            'max_results' => null
        );

        $options = array_merge($defaultOptions, $options);

        if($options['order_by_field'] == 'votes'){
            return $this->getContestantsByVotes($options);
        }


        $qbIds = $this->em->createQueryBuilder();
        $qbIds
            ->select('partial c.{id}')
            ->from('ATMCompetitionBundle:Contestant','c');


        if(!is_null($options['name'])){
            $qbIds->andWhere($qbIds->expr()->like('c.name',$qbIds->expr()->literal('%'.$options['name'].'%')));
        }

        if(!is_null($options['date'])){
            $qbIds->andWhere($qbIds->expr()->lte('c.creation_date',$qbIds->expr()->literal($options['date'])));
        }

        if(!is_null($options['competition'])){
            $qbIds->join('c.competition','comp','WITH',$qbIds->expr()->in('comp.id',$options['competition']));
        }

        if(!is_null($options['ids'])){
            $qbIds->andWhere($qbIds->expr()->in('c.id',$options['ids']));
        }


        $qbIds->orderBy('c.'.$options['order_by_field'],$options['order_by_direction']);


        $pagination = null;
        if(!is_null($options['pagination'])){
            $arrIds = array_map(function($p){
                return $p['id'];
            },$qbIds->getQuery()->getArrayResult());

            $pagination = $this->paginator->paginate(
                $arrIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        }else {
            $query = $qbIds->getQuery();
            if(!is_null($options['max_results'])){
                $query->setMaxResults($options['max_results']);
            }

            $ids = array_map(function ($p) {
                return $p['id'];
            }, $query->getArrayResult());
        }

        $contestants = array();
        if(count($ids) > 0){
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('cont')
                ->addSelect('i')
                ->addSelect('comp')
                ->addSelect('u')
                ->from('ATMCompetitionBundle:Contestant','cont')
                ->join('cont.images','i')
                ->join('cont.competition','comp')
                ->join('cont.user','u')
                ->where($qb->expr()->in('cont.id',$ids));

            if(!is_null($this->config['media_entity']['namespace'])){
                $qb
                    ->addSelect('m')
                    ->leftJoin('cont.media','m');
            }

            $contestants = $qb->getQuery()->getArrayResult();
        }

        return array(
            'results' => $contestants,
            'pagination' => $pagination
        );
    }


    public function getContestantsByVotes($options){
        $competitionVotes = $this->searchVotes->search(array('competition_id'=> $options['competition']));

        $contestantsVotes = array();
        foreach($competitionVotes['results'] as $vote){

            if(!isset($contestantsVotes[$vote->getContestantId()])){
                $contestantsVotes[$vote->getContestantId()] = 0;
            }
            $contestantsVotes[$vote->getContestantId()]++;
        }
        if($options['order_by_direction'] == 'DESC'){
            arsort($contestantsVotes);
        }else{
            asort($contestantsVotes);
        }

        $contestantsIds = array_keys($contestantsVotes);

        if(count($contestantsIds) == 0){
            $options['order_by_field'] = 'name';
            return $this->search($options);
        }

        $pagination = null;
        if(!is_null($options['pagination'])){
            $pagination = $this->paginator->paginate(
                $contestantsIds,
                is_null($options['page']) ? 1 : $options['page'],
                is_null($options['max_results']) ? 10 : $options['max_results']
            );

            $ids = $pagination->getItems();
        }else{
            $ids = $contestantsIds;
        }

        $contestants = array();
        if(count($ids) > 0){
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('cont')
                ->addSelect('i')
                ->addSelect('comp')
                ->addSelect('u')
                ->addSelect('FIELD(cont.id, '.implode(',', $ids).') AS HIDDEN sorting')
                ->from('ATMCompetitionBundle:Contestant','cont')
                ->join('cont.images','i')
                ->join('cont.competition','comp','WITH',$qb->expr()->eq('comp.id',$options['competition']))
                ->join('cont.user','u')
                ->orderBy('sorting','DESC');

            if(!is_null($this->config['media_entity']['namespace'])){
                $qb
                    ->addSelect('m')
                    ->leftJoin('cont.media','m');
            }

            $contestants = $qb->getQuery()->getArrayResult();
        }

        return array(
            'results' => $contestants,
            'pagination' => $pagination
        );
    }
}