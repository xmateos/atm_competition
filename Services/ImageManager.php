<?php

namespace ATM\CompetitionBundle\Services;

use ATM\CompetitionBundle\Helpers\Image as ImageHelper;
use Cake\Filesystem\File;
use \Imagick;

class ImageManager{

    private $rootDir;
    private $config;

    public function __construct($kernel_rootdir,$atm_competition_config)
    {
        $this->rootDir = $kernel_rootdir;
        $this->config = $atm_competition_config;
    }

    public function cropImage($image,$imageDestination,$putWatermark = true,$preserveOriginal = false){
        $tmpFolder = $this->rootDir.'/../web/'.$this->config['media_folder'].'/temp';
        if(!is_dir($tmpFolder)){
            mkdir($tmpFolder);
        }

        $filename = md5(uniqid()).'.'.$image->guessExtension();
        $image->move($tmpFolder,$filename);
        $imageTmpPath = $tmpFolder.'/'.$filename;
        ImageHelper::correctImageOrientation($tmpFolder.'/'.$filename);
        ImageHelper::removeExif($tmpFolder.'/'.$filename);

        if($preserveOriginal){
            copy($imageTmpPath,$imageDestination.'/u_'.$filename);
            ImageHelper::correctImageOrientation($imageDestination.'/u_'.$filename);
            ImageHelper::removeExif($imageDestination.'/u_'.$filename);
        }

        $configWidth = $this->config['main_picture_width'];
        $configHeigh = $this->config['main_picture_height'];

        $imagick = new Imagick($imageTmpPath);
        $imagick->cropThumbnailImage($configWidth,$configHeigh);
        $imagick->writeImage($imageDestination.'/'.$filename);

        if($putWatermark){
            $this->watermarkImage($imageDestination.'/'.$filename,$imageDestination.'/'.$filename);
        }

        return $filename;
    }

    public function watermarkImage($original_file, $destination_file)
    {
        $im = new Imagick($original_file);

        $imageSize = $im->getImageGeometry();
        if($imageSize['width'] == $this->config['main_picture_width'] && $imageSize['height'] == $this->config['main_picture_height']){
            $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_small']);
            $margin = 5;
        }elseif($imageSize['width'] > 1500){
            $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_big']);
            $margin = 10;
        }else{
            $im_watermark = new Imagick($this->rootDir.'/../web/'.$this->config['watermark_image_medium']);
            $margin = 5;
        }

        $watermarkSize = $im_watermark->getImageGeometry();
        $coordinateX = ($imageSize['width'] - $watermarkSize['width']) - $margin;
        $coordinateY = ($imageSize['height'] - $watermarkSize['height']) - $margin;

        $im->compositeImage($im_watermark,Imagick::COMPOSITE_OVER, $coordinateX,$coordinateY);
        $im->writeImage($destination_file);

        return $destination_file;
    }

    public static function resize($original_file, $destination_file, $new_width, $new_height)
    {
        $file = new File($original_file);

        $im = new Imagick($original_file);

        switch($file->mime())
        {
            case 'image/gif':
                $image = $im->coalesceImages();
                foreach($image as $frame)
                {
                    //$frame->cropImage($crop_w,$crop_h,0,0);
                    $frame->thumbnailImage($new_width, $new_height);
                    $frame->setImagePage($new_width, $new_height, 0, 0);
                }
                $image = $image->deconstructImages();
                $image->writeImages($destination_file, true);
                break;
            default:
                $im->resizeImage($new_width, $new_height,Imagick::FILTER_UNDEFINED,1);
                $extension = pathinfo($original_file)['extension'];
                $im->setImageFormat($extension);
                $im->writeImage($destination_file);
                break;
        }

        return $destination_file;
    }

    public static function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 80)
    {
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];

        switch($mime)
        {
            case 'image/gif':
                $image_create = "imagecreatefromgif";
                $image = "imagegif";
                break;
            case 'image/png':
                $image_create = "imagecreatefrompng";
                $image = "imagepng";
                $quality = 7;
                break;
            case 'image/jpeg':
                $image_create = "imagecreatefromjpeg";
                $image = "imagejpeg";
                $quality = 99;
                break;
            default:
                return false;
                break;
        }

        list($width, $height) = getimagesize($source_file);
        if($max_height == 'auto')
        {
            $max_height = $max_width * $height / $width;
        } elseif($max_width == 'auto') {
            $max_width = $max_height * $width / $height;
        }

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if($width_new > $width)
        {
            //cut point by height
            $h_point = (($height - $height_new) / 2);
            //copy image
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        } else {
            //cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        $image($dst_img, $dst_dir, $quality);

        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
        return true;
    }
}