<?php

namespace ATM\CompetitionBundle\Services;

use Doctrine\ODM\MongoDB\DocumentManager;
use Knp\Component\Pager\PaginatorInterface;
use ATM\CompetitionBundle\Document\CompetitionVote;
use \MongoRegex;
use \MongoDate;

class SearchVotes{

    private $dm;
    private $paginator;


    public function __construct(DocumentManager $dm,PaginatorInterface $paginator)
    {
        $this->dm = $dm;
        $this->paginator = $paginator;
    }


    public function search($options){
        $defaultOptions = array(
            'email' => null,
            'competition_id' => null,
            'contestant_id' => null,
            'creation_date' => null,
            'ids' => null,
            'sorting' => 'DESC',
            'sorting_field' => null,
            'date_limit' => array(
                'min' => null,
                'max' => null
            ),
            'limit' => null,
            'hydrate' => true,
            'page' => 1,
            'max_results' => null,
            'pagination' => null,
            'count' => null
        );

        $options = array_merge($defaultOptions, $options);

        $qb = $this->dm->createQueryBuilder(CompetitionVote::class);
        $qb->select('id','email','competition_id','contestant_id','creation_date');

        if(!is_null($options['email'])){
            $qb->field('email')->equals(new MongoRegex("/^".$options['email']."/"));
        }

        if(!is_null($options['competition_id'])){
            $qb->field('competition_id')->equals((int)$options['competition_id']);
        }

        if(!is_null($options['contestant_id'])){
            $qb->field('contestant_id')->equals((int)$options['contestant_id']);
        }

        if(!is_null($options['date_limit']['min'])){
            $min_date = $options['date_limit']['min'];
            $qb->field('creation_date')->gte(new MongoDate($min_date->format('U'),$min_date->format('u')));
        }

        if(!is_null($options['date_limit']['max'])){
            $max_date = $options['date_limit']['max'];
            $qb->field('creation_date')->lte(new MongoDate($max_date->format('U'),$max_date->format('u')));
        }

        if(!is_null($options['limit'])){
            $qb->limit($options['limit']);
        }

        if(is_null($options['pagination'] && $options['max_results'])){
            $qb->limit($options['max_results']);
        }
        $query = $qb->hydrate($options['hydrate'])->getQuery();


        if(!is_null($options['count'])){
            return array('count'=>$query->execute()->count());
        }else{
            $pagination = null;
            if(!is_null($options['pagination'])){
                $pagination = $this->paginator->paginate(
                    $query->execute()->toArray(),
                    $options['page'],
                    $options['max_results']
                );
                $votes = $pagination->getItems();
            }else {
                $votes = $query->execute();
            }

            return array(
                'results' => $votes,
                'pagination' => $pagination
            );
        }
    }
}